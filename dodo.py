DOIT_CONFIG = {'default_tasks': ['all']}


def task_gitclean():
    """Clean all generated files not tracked by GIT."""
    return {
            'actions': ['git clean -xdf'],
           }


def task_html():
    """Make HTML documentation."""
    return {
            'actions': ['make html'],
           }


def task_migrate():
    """Create database."""
    return {
            'actions': ['python3 tracker/manage.py migrate'],
           }


def task_po():
    """Update translations."""
    return {
            'actions': ['django-admin makemessages -l ru'],
            'file_dep': ['tracker/locale/ru/LC_MESSAGES/django.po'],
           }


def task_mo():
    """Compile translations."""
    return {
        'actions': ['django-admin compilemessages'],
        'file_dep': ['tracker/locale/ru/LC_MESSAGES/django.po'],
        'task_dep': ['po'],
    }


def task_app():
    """Run server at http://127.0.0.1:8000/."""
    return {
            'actions': ['python3 tracker/manage.py runserver'],
            'task_dep': ['migrate', 'mo'],
           }


def task_style():
    """Check style against flake8."""
    return {
            'actions': ['flake8 tracker']
           }


def task_docstyle():
    """Check docstrings against pydocstyle."""
    return {
            'actions': ['pydocstyle tracker']
           }


def task_check():
    """Perform all checks."""
    return {
            'actions': None,
            'task_dep': ['style', 'docstyle']
           }


def task_all():
    """Perform all build task."""
    return {
            'actions': None,
            'task_dep': ['gitclean', 'check', 'html', 'mo']
           }
