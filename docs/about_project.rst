Habit Tracker
=========================================

Описание проекта
_________________________________________

Habit Tracker - это веб-приложение, для отслеживания регулярности выполнения "привычек".

Предполагаемый функционал:

- Регистрация пользователя
- Добавление пользовательского списка привычек
- Отметка выполненных целей за день, а также просмотр статистики за определенный срок
- Просмотр рейтинга пользователей по выполненным целям
- Добавление друзей

Дополнительно:

- Возможность настройки личного профиля, например, добавление аватара
- Просмотр рейтинга друзей
- Отправление пользователю напоминаний
- ...

Используемые инструменты
____________________________________________

- Django
- ...

Макет интерфейса
_________________________________________

Предварительный вид главной страницы со списком "привычек":

|index|

.. |index| image:: ../tracker/app/static/app/index_example.png

Запуск программы
_________________________________________

После установки из колеса(.whl) при первом запуске выполнить команду создающую базу данных: ::

    python3 -m HabitTracker.manage migrate

Затем запуск сервера: ::

    python3 -m HabitTracker.manage runserver


Запуск тестов
_________________________________________

В репозитории выполнить: ::

    cd tracker
    python3 manage.py test

