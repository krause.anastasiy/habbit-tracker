Auth
=====

.. automodule:: app.auth

    .. autofunction:: user_login(request)
    .. autofunction:: register(request)
