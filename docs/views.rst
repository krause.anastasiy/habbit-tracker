Views
=====

.. automodule:: app.views

    .. autofunction:: index(request, user_id)
    .. autofunction:: add_habit(request, user_id)
