.. Habit Tracker documentation master file, created by
   sphinx-quickstart on Sun Apr  2 21:06:37 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Habit Tracker's documentation!
=========================================

.. toctree::
   :maxdepth: 2

   about_project


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
