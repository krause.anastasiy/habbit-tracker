
[[_TOC_]]

# Habit Tracker

## Постановка решаемой задачи

Веб-приложение, для отслеживания регулярности выполнения "привычек".

Предполагаемый функционал:

- Регистрация пользователя
- Добавление пользовательского списка привычек
- Отметка выполненных целей за день, а также просмотр статистики за определенный срок
- Просмотр рейтинга пользователей по выполненным целям
- Добавление друзей

Дополнительно:

- Возможность настройки личного профиля, например, добавление аватара
- Просмотр рейтинга друзей
- Отправление пользователю напоминаний
- ...

## Описание предполагаемых инструментов решения

- Django
- ...

## Макет интерфейса

<details><summary>Предварительный вид главной страницы со списком "привычек"</summary>
![alt text](tracker/app/static/app/index_example.png "Страница со списком привычек и их выполнением на протяжении последней недели")
</details>

## Запуск программы

После установки из колеса(.whl) при первом запуске выполнить команду создающую базу данных:

```python3 -m HabitTracker.manage migrate```

Затем запуск сервера:

```python3 -m HabitTracker.manage runserver```


## Запуск тестов

В репозитории выполнить:

```angular2html
    cd tracker
    python3 manage.py test
```

