from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime, timedelta


class User(AbstractUser):
    friends = models.ManyToManyField("self", blank=True, symmetrical=True)


class FriendRequest(models.Model):
    from_user = models.ForeignKey(
        User,
        related_name="from_user",
        on_delete=models.CASCADE)
    to_user = models.ForeignKey(
        User,
        related_name="to_user",
        on_delete=models.CASCADE)


class Habit(models.Model):
    name = models.CharField(max_length=256, null=False, blank=False)
    description = models.CharField(max_length=1024)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def get_checks_for_week(self):
        """Return the days then habit was completed."""
        result = []
        current_date = datetime.today()
        for i in range(1, 7):
            day = current_date - timedelta(days=i)
            result.append(self.habitchecks_set.filter(date=day))
        return result

    def get_check_for_day(self, current_time):
        """Check if the habit is completed in a day."""
        return self.habitchecks_set.filter(date=current_time)

    def is_checked_today(self):
        """Check if the habit is completed today."""
        return self.habitchecks_set.filter(date=datetime.today())


class HabitChecks(models.Model):
    date = models.DateField(auto_now_add=False, auto_now=False)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    habit = models.ForeignKey(Habit, on_delete=models.CASCADE)
