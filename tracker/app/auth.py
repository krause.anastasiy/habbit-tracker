from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from .forms import LoginForm, RegistrationForm
from .models import User


def user_login(request):
    """
    Authenticate user.

        request: HTTPRequest
    """
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            user = authenticate(username=cleaned_data['username'],
                                password=cleaned_data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)

                    return HttpResponseRedirect(
                        reverse('index', args=()))

            else:
                # TODO: Display error about invalid login
                return render(request, 'app/login.html', {'form': form})
    else:
        form = LoginForm()

    return render(request, 'app/login.html', {'form': form})


def register(request):
    """
    Register user.

        request: HTTPRequest
    """
    if request.method == 'POST':
        user = RegistrationForm(request.POST)
        if user.is_valid():
            new_user = user.save(commit=False)
            new_user.set_password(user.cleaned_data['password'])
            new_user.save()

            return HttpResponseRedirect(reverse('login', args=()))
    else:
        user = RegistrationForm()

    return render(request, 'app/register.html', {'form': user})


@login_required
def user_logout(request):
    user = User.objects.get(pk=request.user.id)

    if user and user.is_active:
        logout(request)

    return HttpResponseRedirect(reverse('login', args=()))
