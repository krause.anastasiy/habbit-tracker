from django.test import TestCase, RequestFactory
from .models import User, FriendRequest, Habit, HabitChecks
from . import views
from datetime import datetime, timedelta


class UserTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.Alice = User.objects.create(username="Alice")
        self.Bob = User.objects.create(username="Bob")
        self.Mallory = User.objects.create(username="Mallory")

    def test_friends_field(self):
        self.Alice.friends.add(self.Bob)
        self.Alice.friends.add(self.Mallory)

        got = set(self.Alice.friends.all())
        expected = set([self.Bob, self.Mallory])
        self.assertEqual(got, expected)

        got = set(self.Bob.friends.all())
        expected = set([self.Alice])
        self.assertEqual(got, expected)

        got = set(self.Mallory.friends.all())
        expected = set([self.Alice])
        self.assertEqual(got, expected)

    def test_friend_request_accepted(self):
        # request from Alice to Bob
        request = self.factory.get("/")
        request.user = self.Alice
        views.send_friend_request(request, self.Bob.id)
        fr = FriendRequest.objects.filter(
            from_user=self.Alice,
            to_user=self.Bob)
        self.assertTrue(fr.exists())

        request.user = self.Bob
        views.accept_friend_request(request, self.Alice.id)
        fr = FriendRequest.objects.filter(
            from_user=self.Alice,
            to_user=self.Bob)
        self.assertFalse(fr.exists())
        self.assertTrue(self.Alice.friends.all().contains(self.Bob))

    def test_friend_request_declined(self):
        # request from Alice to Bob
        request = self.factory.get("/")
        request.user = self.Alice
        views.send_friend_request(request, self.Bob.id)
        fr = FriendRequest.objects.filter(
            from_user=self.Alice,
            to_user=self.Bob)
        self.assertTrue(fr.exists())

        request.user = self.Bob
        views.decline_friend_request(request, self.Alice.id)
        fr = FriendRequest.objects.filter(
            from_user=self.Alice,
            to_user=self.Bob)
        self.assertFalse(fr.exists())
        self.assertFalse(self.Alice.friends.all().contains(self.Bob))


class HabitCheckerTestCase(TestCase):
    def setUp(self):
        self.Alice = User.objects.create(username="Alice")
        self.TestHabit = Habit.objects.create(
            name="test habit",
            description="test habit description",
            user=self.Alice,
        )

    def create_habit_check(self, time):
        return HabitChecks.objects.create(
            user=self.Alice,
            habit=self.TestHabit,
            date=time
        )

    def test_current_day_check(self):
        self.create_habit_check(datetime.today())

        self.assertTrue(self.TestHabit.is_checked_today())

    def test_yesterday_check(self):
        self.assertFalse(
            self.TestHabit.get_check_for_day(
                datetime.today() - timedelta(days=1)))
