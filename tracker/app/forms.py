from django import forms
from .models import User


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        fields = ['username', 'password']
        for field in fields:
            self.fields[field].widget.attrs['class'] = 'default-input'


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat password',
                                widget=forms.PasswordInput)

    class Meta:  # noqa: D106
        model = User
        fields = ('username', 'first_name', 'email')

    def __init__(self, *args, **kwargs):  # noqa: D107
        super().__init__(*args, **kwargs)
        fields = ['username', 'first_name', 'email', 'password', 'password2']
        for field in fields:
            self.fields[field].widget.attrs['class'] = 'default-input'

    def clean_password2(self):
        """Validate a re-entered password."""
        # TODO: Use this function and show error to user
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']
