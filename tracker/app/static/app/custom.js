function toggle_edit_block() {
    let block = document.getElementById('edit-block');

    if (block.style.display === 'none') {
        block.style.display = 'block';
    } else {
        block.style.display = 'none';
    }
}