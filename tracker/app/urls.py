from django.urls import path

from . import views, auth

urlpatterns = [
    path('', auth.user_login, name='login'),
    path('logout/', auth.user_logout, name='logout'),
    path('register/', auth.register, name='register'),
    path('index/', views.index, name='index'),
    path('index/habit/add', views.add_habit, name='add_habit'),
    path('index/habit/delete/<int:habit_id>/', views.delete_habit,
         name='delete_habit'),
    path('index/check', views.check_today_habits, name='check'),
    path(
        'send_friend_request/<int:user_id>/',
        views.send_friend_request,
        name='send_friend_request',
    ),
    path(
        'accept_friend_request/<int:user_id>/',
        views.accept_friend_request,
        name='accept_friend_request',
    ),
    path(
        'decline_friend_request/<int:user_id>/',
        views.decline_friend_request,
        name='decline_friend_request',
    ),
]
