from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from .models import User, Habit, FriendRequest, HabitChecks
from datetime import datetime, timedelta
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .constants import Constants


@login_required
def index(request):
    """
    Generate index page.

        request: HTTPRequest
    """
    user = User.objects.get(pk=request.user.id)
    habits = user.habit_set.all()

    current_date = datetime.today()
    last_week = []
    for i in range(7):
        day = current_date - timedelta(days=i)
        last_week.append(Constants.WEEKDAY[day.weekday()])

    context = {
        'user': user,
        'date': current_date.strftime('%Y-%m-%d'),
        'habits': habits,
        'days': last_week
    }
    return render(request, 'app/index.html', context)


@login_required
def add_habit(request):
    """
    Add new habit to current users habit list.

        request: HTTPRequest
    """
    user = get_object_or_404(User, pk=request.user.id)
    name = None

    try:
        name = request.POST.get('habit-name')
    except Exception as e:
        messages.error(request, 'Can not add new habit: {0}'.format(str(e)))

    if name:
        habit = Habit(name=name)
        habit.user = user
        habit.save()

    else:
        messages.warning(request, 'Empty name')

    return HttpResponseRedirect(reverse('index', args=()))


def delete_habit(request, habit_id):
    habit = get_object_or_404(Habit, pk=habit_id)
    habit.delete()

    return HttpResponseRedirect(reverse('index', args=()))


@login_required()
def check_today_habits(request):
    user = get_object_or_404(User, pk=request.user.id)
    current_date = datetime.today()
    checks = []
    try:
        checks = request.POST.getlist("checks")
    except Exception as e:
        messages.error(request, 'Can not add today checks: {0}'.format(str(e)))

    for check in checks:
        habit = get_object_or_404(Habit, name=check)
        if not habit.is_checked_today():
            new_habit_check = HabitChecks(user=user,
                                          date=current_date,
                                          habit=habit)
            new_habit_check.save()

    for habit in user.habit_set.all():
        if habit.name not in checks:
            old_habit_check = HabitChecks.objects.filter(habit=habit,
                                                         date=current_date)
            if old_habit_check:
                old_habit_check.delete()

    return HttpResponseRedirect(reverse('index', args=()))


@login_required
def send_friend_request(request, user_id):
    from_user = request.user
    to_user = get_object_or_404(User, id=user_id)
    FriendRequest.objects.get_or_create(from_user=from_user, to_user=to_user)
    return HttpResponse("Friend request sent")


@login_required
def accept_friend_request(request, user_id):
    to_user = request.user
    from_user = get_object_or_404(User, id=user_id)
    fr = get_object_or_404(FriendRequest, from_user=from_user, to_user=to_user)
    to_user.friends.add(from_user)
    fr.delete()
    return HttpResponse("Friend request accepted")


@login_required
def decline_friend_request(request, user_id):
    to_user = request.user
    from_user = get_object_or_404(User, id=user_id)
    fr = get_object_or_404(FriendRequest, from_user=from_user, to_user=to_user)
    fr.delete()
    return HttpResponse("Friend request declined")
