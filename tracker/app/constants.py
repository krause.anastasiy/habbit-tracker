from django.utils.translation import gettext_lazy as _


class Constants:
    WEEKDAY = [_("Monday"), _("Tuesday"),
               _("Wednesday"), _("Thursday"),
               _("Friday"), _("Saturday"),
               _("Sunday")]
